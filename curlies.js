/**
 * Script to replace boring quotes with curly quotes. 
 *
 * Unicode characters, plain
 * " U+0022
 * ' U+0027
 *
 * Unicode characters, curly
 * ‘ U+2018
 * ’ U+2019
 * “ U+201c
 * ” U+201d
 */
(function ($) {
  $.fn.curlies = function( itemQuery ) {
    function smarten(text) {
      console.log(text);
      return text
        /* opening singles */
        .replace(/(^|[-\u2014\s(\["])'/g, "$1\u2018")

        /* closing singles & apostrophes */
        .replace(/'/g, "\u2019")

        /* opening doubles */
        .replace(/(^|[-\u2014/\[(\u2018\s])"/g, "$1\u201c")

        /* closing doubles */
        .replace(/"/g, "\u201d")

        /* em-dashes */
        .replace(/--/g, "\u2014");
    };

    return this.each(function(){
      var $this = $(this);
      var $children = $this.children();

      // If there are children, loop again (skip code tags).
      if ($children.not('code').length) {
        $children.not('code').each(function(){
          $(this).curlies();
        });
      }
      // If there are no child elements, replace text.
      else {
        if ($this.text().match(/([-\u2014\s(\["])|(')|(([-\u2014/\[(\u2018\s])")|(")|(--)/)) {
          $this.text( smarten( $this.text() ) );
        }
      }
    });
  };
})(jQuery);
